package com.example.mytaxi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main3Activity<name> extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private EditText mnameField, msurnameField, mphoneField,mnumber;
    private DatabaseReference database;
    private Button button;
    private Button getButton;

    int in;
    SharedPreferences prefs;

    EditText name;
    String nameString;
    EditText surname;
    String surnameString;
    EditText mobile;
    String mobileString;
    EditText pplNo;
    String pplNoString;

    Date today;
    SimpleDateFormat format;
    String DateTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main3);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav1);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        database = FirebaseDatabase.getInstance().getReference("Customer");
        mnameField = (EditText) findViewById(R.id.inputname);
        msurnameField = (EditText) findViewById(R.id.inputsur);
        mphoneField = (EditText) findViewById(R.id.mobnumber);
        mnumber=(EditText)findViewById(R.id.inputppl);

         getButton=(Button) findViewById(R.id.del);
/*---------------------------------------------------------------------------------------------------------------------------------
         getButton.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 deleteBooking(nameString);
                 Toast.makeText(Main3Activity.this, "Bookings made from this phone deleted", Toast.LENGTH_SHORT).show();
             }
         });*/






        button = (Button) findViewById(R.id.contbutton);

        NavigationView navigationView1 = (NavigationView)findViewById(R.id.nav1);
        navigationView1.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int nav = menuItem.getItemId();

                if (nav == R.id.map){
                    startActivity(new Intent(Main3Activity.this, MapsActivity.class));
                }
                if (nav == R.id.vehicle){
                    startActivity(new Intent(Main3Activity.this, Vehicle.class));
                }
                if (nav == R.id.contact){
                    startActivity(new Intent(Main3Activity.this,Contactus.class));
                }
                return true;
            }
        });

        prefs = PreferenceManager.getDefaultSharedPreferences(Main3Activity.this);

        name = findViewById(R.id.inputname);
        surname = findViewById(R.id.inputsur);
        mobile = findViewById(R.id.mobnumber);
        pplNo = findViewById(R.id.inputppl);
    }

    private void deleteBooking(String nameString) {
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference("Customer").child(nameString);

        databaseReference.removeValue();
    }


    public void ClickVehicle (View v){
        openvehicle();
    }

    public void openvehicle () {
        Intent intent = new Intent(Main3Activity.this, Vehicle.class);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main3, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav1) {
            // Handle the camera action
            if (id == R.id.map) {

            } else if (id == R.id.vehicle) {
                openvehicle();
                Toast.makeText(Main3Activity.this, "Vehicle", Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(Main2Activity.this, Vehicle.class));

            } else if (id == R.id.contact) {

            }



            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);

        }

        return  true;
    }

//---------------------------------------------------------------------------------------------------------------------------
    public void openActivity3(View v) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
        SaveUserInfo();
    }

    public void Delete(View v){
        deleteBooking(nameString);
        Toast.makeText(Main3Activity.this, "Bookings made from this phone deleted", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor Spref = prefs.edit();

        nameString = name.getText().toString();
        Spref.putString("name", nameString);

        surnameString = surname.getText().toString();
        Spref.putString("surname", surnameString);

        mobileString = mobile.getText().toString();
        Spref.putString("mobile", mobileString);

        pplNoString = pplNo.getText().toString();
        Spref.putString("pplNo", pplNoString);

        Spref.apply();
        Spref.putString ("time", DateTime);
        today = new Date();
        format = new SimpleDateFormat("dd-MM-yyyy,hh:mm:ss a");
        DateTime=format.format(today);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor Spref = prefs.edit();

        nameString = name.getText().toString();
        Spref.putString("name", nameString);

        surnameString = surname.getText().toString();
        Spref.putString("surname", surnameString);

        mobileString = mobile.getText().toString();
        Spref.putString("mobile", mobileString);

        pplNoString = pplNo.getText().toString();
        Spref.putString("pplNo", pplNoString);

        Spref.apply();

        Spref.putString ("time", DateTime);

        today = new Date();
        format = new SimpleDateFormat("dd-MM-yyyy,hh:mm:ss a");
        DateTime=format.format(today);
    }

    @Override
    protected void onResume() {
        super.onResume();
        nameString = prefs.getString("name", "");
        name.setText(nameString);

        surnameString = prefs.getString("surname", "");
        surname.setText(surnameString);

        mobileString = prefs.getString("mobile", "");
        mobile.setText(mobileString);

        pplNoString = prefs.getString("pplNo", "");
        pplNo.setText(pplNoString);

        View parentLayout = findViewById(android.R.id.content);
        setContentView(R.layout.activity_main3);
        Snackbar.make(parentLayout, DateTime, Snackbar.LENGTH_LONG).setAction("Close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        nameString = prefs.getString("name", "");
        name.setText(nameString);

        surnameString = prefs.getString("surname", "");
        surname.setText(surnameString);

        mobileString = prefs.getString("mobile", "");
        mobile.setText(mobileString);

        pplNoString = prefs.getString("pplNo", "");
        pplNo.setText(pplNoString);
        View parentLayout = findViewById(android.R.id.content);
        setContentView(R.layout.activity_main3);
        Snackbar.make(parentLayout, DateTime, Snackbar.LENGTH_LONG).setAction("Close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).show();

    }

    private void SaveUserInfo() {
        String name = mnameField.getText().toString().trim();
        String surname = msurnameField.getText().toString().trim();
        String phone = mphoneField.getText().toString().trim();

        if (!TextUtils.isEmpty(name)) {
            String id = database.push().getKey();
            UserInfo userInfo = new UserInfo(name, surname, phone);
            database.child(id).setValue(userInfo);
            Toast.makeText(this, "Booking added", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Cannot be Empty", Toast.LENGTH_LONG).show();
        }


    }
}

