package com.example.mytaxi;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.nightonke.blurlockview.BlurLockView;
import com.nightonke.blurlockview.Directions.HideType;
import com.nightonke.blurlockview.Eases.EaseType;
import com.nightonke.blurlockview.Password;

public class pincode extends AppCompatActivity {

    private BlurLockView blurLockView;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pincode);

        blurLockView=(BlurLockView)findViewById(R.id.blurLockView);

        blurLockView.setCorrectPassword("2405");
        blurLockView.setLeftButton("Left");
        blurLockView.setRightButton("Right");
        blurLockView.setTypeface(Typeface.DEFAULT);
        blurLockView.setType(Password.NUMBER,false);






        blurLockView.setOnLeftButtonClickListener(new BlurLockView.OnLeftButtonClickListener() {
            @Override
            public void onClick() {
                Toast.makeText(pincode.this, "Left Button Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        blurLockView.setOnPasswordInputListener(new BlurLockView.OnPasswordInputListener() {
            @Override
            public void correct(String inputPassword) {
                Toast.makeText(pincode.this,"Correct password",Toast.LENGTH_SHORT).show();
                blurLockView.hide(1000, HideType.FADE_OUT, EaseType.EaseInBounce);
                openActivity1();

            }

            @Override
            public void incorrect(String inputPassword) {
                Toast.makeText(pincode.this,"Incorrect password",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void input(String inputPassword) {

            }
        });


    }

    private void openActivity1() {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
