package com.example.mytaxi;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class Main2Activity extends AppCompatActivity {

    private EditText mnameField, msurnameField, mphoneField;
    private DatabaseReference database;


    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mtoggle;
    private Button button;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mtoggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.Open, R.string.Close);
        mtoggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mtoggle);
        mtoggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        database = FirebaseDatabase.getInstance().getReference("Customer");
        mnameField = (EditText) findViewById(R.id.inputname);
        msurnameField = (EditText) findViewById(R.id.inputsur);
        mphoneField = (EditText) findViewById(R.id.mobnumber);


        NavigationView nav = (NavigationView) findViewById(R.id.nav);
        nav.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            public void ClickVehicle(View v) {
                openvehicle();
            }

            public void openvehicle() {
                Intent intent = new Intent(Main2Activity.this, Vehicle.class);
                startActivity(intent);

            }

            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.map) {

                }
                if (id == R.id.vehicle) {
                    openvehicle();
                    Toast.makeText(Main2Activity.this, "Vehicle", Toast.LENGTH_SHORT).show();
                    //startActivity(new Intent(Main2Activity.this, Vehicle.class));

                }
                if (id == R.id.contact) {

                }

                return true;
            }
        });

        button = (Button) findViewById(R.id.contbutton);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity3();
            }
        });

        button = (Button) findViewById(R.id.Test);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity4();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mtoggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    public void openActivity3() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
        SaveUserInfo();

    }

    public void openActivity4() {
        Intent intent = new Intent(this, Vehicle.class);
        startActivity(intent);


    }


    private void SaveUserInfo() {
        String name = mnameField.getText().toString().trim();
        String surname = msurnameField.getText().toString().trim();
        String phone = mphoneField.getText().toString().trim();

        if (!TextUtils.isEmpty(name)) {
            String id = database.push().getKey();
            UserInfo userInfo = new UserInfo(name, surname, phone);
            database.child(id).setValue(userInfo);
            Toast.makeText(this, "Booking added", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Cannot be Empty", Toast.LENGTH_LONG).show();
        }


    }



}

    //@Override
    //protected void onStart() {
        //super.onStart();

        //database.addValueEventListener(new ValueEventListener() {
           // @Override
            //public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               // userInfoList.clear();

               // for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                   // UserInfo userInfo = dataSnapshot1.getValue(UserInfo.class);

                    //userInfoList.add(userInfo);


                //}

            //}

           //@Override
            //public void onCancelled(@NonNull DatabaseError databaseError) {

           // }
       // });


