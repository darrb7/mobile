package com.example.mytaxi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class Vehicle extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle);

        ArrayList<ListItems> listItems = new ArrayList<>();
        listItems.add(new ListItems(R.drawable.mercedes, "Mercedes C300"));
        listItems.add(new ListItems(R.drawable.bmw, "Bmw 320D"));
        listItems.add(new ListItems(R.drawable.toyota, "Toyota Corolla"));
        listItems.add(new ListItems(R.drawable.mitsubishi, "Mitsubishi Lancer SE"));
        listItems.add(new ListItems(R.drawable.bora, "Volkswagen Bora"));
        listItems.add(new ListItems(R.drawable.peugeot408jpg,"Peugeot 408"));
        listItems.add(new ListItems(R.drawable.octavia,"Skoda Octavia"));
        listItems.add(new ListItems(R.drawable.ford,"Ford Mondeo"));
        recyclerView = findViewById(R.id.vehiclelist);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        adapter = new Adapter(listItems);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
