package com.example.mytaxi;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.Holder> {
    private ArrayList<ListItems> listItems;

    public static class Holder extends RecyclerView.ViewHolder{
        public ImageView mImageView;
        public TextView mTextView1;

        public Holder(@NonNull View itemView) {
            super(itemView);
            mImageView =itemView.findViewById(R.id.imageView);
            mTextView1=itemView.findViewById(R.id.textView);
        }
    }

    public Adapter(ArrayList<ListItems>exampleList){
        listItems = exampleList;
    }

    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cardview,viewGroup,false);
        Holder evh = new Holder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
      ListItems currentItem= listItems.get(position);

      holder.mImageView.setImageResource(currentItem.getmImage());
      holder.mTextView1.setText(currentItem.getmText1());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }
}

